import './App.css';
import { Container, Button, ButtonGroup, Grid } from '@mui/material'
import { useState } from 'react';
import Login from './components/Login';
import SignUp from './components/SignUp';
import { grey } from '@mui/material/colors';

const style = {
  backgroundColor: 'gray',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center'
}

function App() {
  const [isLogin, setIsLogin] = useState(true)
  return (
    <Container sx={{ height: '300px' }}>
      <Grid container sx={style}>
        <Grid item xs={12} sm={12} md={12} lg={12} >
          <ButtonGroup variant="contained" mt={5} aria-label="outlined primary button group" sx={style}>
            <Button color={isLogin ? 'inherit' : "success"} onClick={() => setIsLogin(false)}>SIGN UP</Button>
            <Button color={isLogin ? 'success' : "inherit"} onClick={() => setIsLogin(true)}>LOGIN</Button>
          </ButtonGroup>
        </Grid>
        {
          isLogin ? <Login /> : <SignUp />
        }
      </Grid>
    </Container>
  );
}

export default App;
