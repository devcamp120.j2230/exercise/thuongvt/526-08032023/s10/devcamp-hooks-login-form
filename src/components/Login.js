import { Button, Grid, TextField, Typography } from '@mui/material'
import React, { useState } from 'react'

function Login() {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const handleSignUp = () => {
    if(!username){
      alert("Bạn chưa nhập user name");
      return false
    }
    if(!password){
      alert("Bạn chưa nhập passWord");
      return false
    }
    else{
      console.log(username,password)
    }
   }
  return (
    <Grid container mt={3} sx={{ alignContent: 'center', justifyContent: 'center', flexDirection: 'column' }}>
      <Grid item >
        <Typography variant="h3" component='div'>
          Welcome Back!
        </Typography>
      </Grid>
      <Grid item >
        <TextField id='inp-username' label='email' variant='outlined' onChange={(e) => setUsername(e.target.value)} value={username} />
      </Grid>
      <Grid item >
        <TextField id='inp-password' label='password' variant='outlined' onChange={(e) => setPassword(e.target.value)} value={password} />
      </Grid>
      <Grid>
        <Button id='btn-login' variant='contained' onClick={(e) => handleSignUp(e)}>LOGIN</Button>
      </Grid>
    </Grid>
  )
}

export default Login