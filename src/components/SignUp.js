import { Button, Grid, TextField, Typography } from '@mui/material'
import React, { useState } from 'react'

function SignUp() {
  const [firstname, setFirstname] = useState('')
  const [lastname, setLastname] = useState('')
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const handleSignUp = () => { 
    if(!firstname){
      alert("Bạn chưa nhập firstname");
      return false
    }
    if(!lastname){
      alert("Bạn chưa nhập lastname");
      return false
    }
    if(!username){
      alert("Bạn chưa nhập username");
      return false
    }
    if(!password){
      alert("Bạn chưa nhập passWord");
      return false
    }
    else{
      console.log(firstname,lastname,username,password)
    }
  }
  return (
    <Grid container mt={3} sx={{ alignContent: 'center', justifyContent: 'center', flexDirection: 'column' }}>
      <Grid item >
        <Typography variant="h3" component='div'>
          Welcome Back!
        </Typography>
      </Grid>

      <Grid item >
        <TextField id='inp-firstname' label='FirstName' variant='outlined' onChange={(e) => setFirstname(e.target.value)} value={firstname} />
      </Grid>
      <Grid item >
        <TextField id='inp-lastname' label='LastName' variant='outlined' onChange={(e) => setLastname(e.target.value)} value={lastname} />
      </Grid>
      <Grid item >
        <TextField id='inp-username' label='email' variant='outlined' onChange={(e) => setUsername(e.target.value)} value={username} />
      </Grid>
      <Grid item >
        <TextField id='inp-password' label='password' variant='outlined' onChange={(e) => setPassword(e.target.value)} value={password} />
      </Grid>
      <Grid>
        <Button id='btn-sign-up' variant='contained' onClick={(e) => handleSignUp(e)}>SIGN UP</Button>
      </Grid>
    </Grid>
  )
}

export default SignUp